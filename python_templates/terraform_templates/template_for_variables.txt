variable "zone" {
  default = "{{ zone }}"
}

variable "token" {
  default = "{{ token }}"
}

variable "cloud-id" {
  default = "{{ cloud_id }}"
}

variable "folder-id" {
  default = "{{ folder_id }}"
}

variable "os-image" {
  # Centos 7
  default = "fd8p7vi5c5bbs2s5i67s"
}

variable "data-file" {
  default = "meta.txt"
}