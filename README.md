# Netcracker graduation project

### Installation

To download repository write this in your terminal:
```
git clone https://gitlab.com/sweetysweat/netcraker-devops-project.git
```
After that it's better to create virtual environment and run these commands to install requirements:
```
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
deactivate
source venv/bin/activate
```
### Launch
Our project at this moment supports two program operating modes:
1. Manual. All needed files will be created during the process (You have to write all creds and passwords in terminal).
2. Automatic (all files with creds have to be created before running this code in netcracker-devops-project directory). Needed files:

   - terraform/variables.tf
   - ssh_keys/service.key
   - ssh_keys/service.key.pub
   - terraform/meta.txt (here you should write your public key)
   - ansible_jenkins_for_python/vault_pass.txt (Write me down to get password, but you can always change data in ansible dir).

To run the code you can use this command in your terminal (or you can try `python3` if you didn't create venv):
```
python start_up.py
```

---
### Plan for nearest future:

- Change wizard (skip it) for jenkins (create user)
- Learn creds for jenkins
- Run jobs from git
- Learn docker
- Oracle DB (optional)
- Kuber (optional)